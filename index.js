const http = require('http');
const hdb = require('hdb');

/**
 * DB DETAILS
 * host: 172.25.221.40
 * username: SYSTEM
 *** @description: username SYSTEM is default value. Read more how to add/change this part from sap hana documentations 
 * password
 *** @description: password is configure by our side. Its customize by us. Just make sure this part is secure enough
 */

http.createServer(function (req, res) {
    const client = hdb.createClient({
        host     : '172.25.221.40',
        port     : 39015,
        user     : 'SYSTEM',
        password : '*********'
      });

      client.on('error', function (err) {
        console.error('Network connection error', err);
      });

      client.connect(function (err) {
        if (err) {
            return console.error('Connect error', err);
        }

        client.exec('select * from DUMMY', function (err, rows) {
          client.end();
          if (err) {
            return console.error('Execute error:', err);
          }
          console.log('Results:', rows);
        });
      });

  res.write('Hello World!'); //write a response to the client
  res.end(); //end the response
}).listen(7030); //the server object listens on port 8080